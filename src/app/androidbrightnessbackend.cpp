/*
    Copyright (C) 2018 Nicolas Fella <nicolas.fella@gmx.de>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "androidbrightnessbackend.h"

#include <QDebug>
#include <QtAndroid>
#include <QAndroidJniObject>

AndroidBrightnessBackend::AndroidBrightnessBackend(QObject *parent)
    : BrightnessBackend(parent)
{
}

AndroidBrightnessBackend::~AndroidBrightnessBackend()
{
}

float AndroidBrightnessBackend::brightness() const
{

    float brightness = QtAndroid::androidActivity().callMethod<jfloat>("getBrightness", "()F");

    return brightness;
}

void AndroidBrightnessBackend::setBrightness(float brightness)
{
    const auto activity = QtAndroid::androidActivity();
    if (activity.isValid()) {
        activity.callMethod<void>("setBrightness", "(F)V", brightness);
    }
}

float AndroidBrightnessBackend::maxBrightness() const
{
    return 1;
}
